CREATE SCHEMA aviation;
SET SEARCH_PATH to aviation;

CREATE DOMAIN flightnum varchar(6);

CREATE DOMAIN aircode char(3);

CREATE TABLE Airplane_Type(
type_name varchar(20),
max_seats smallint not null,
company varchar(30) not null,
PRIMARY KEY(type_name)
);

CREATE TABLE airplane (
airplane_id char(5),
airplane_type varchar(20) not null,
PRIMARY KEY (airplane_id),
FOREIGN KEY (airplane_type) REFERENCES Airplane_Type(type_name)on DELETE RESTRICT on UPDATE RESTRICT 
);

CREATE TABLE airport(
airport_code aircode,
name varchar(40) not null,
city varchar(25) not null,
state varchar (25) not null,
country varchar (25) not null,
PRIMARY KEY (airport_code)
);

CREATE TABLE flight(
flight_num flightnum,
airline varchar(25) not null,
weekdays varchar(10) not null,
dept_airport aircode not null,
sch_dept_time time not null,
arr_airport aircode not null,
sch_arr_time time not null,
PRIMARY KEY (flight_num),
FOREIGN KEY (dept_airport) REFERENCES airport(airport_code),
FOREIGN KEY (arr_airport) REFERENCES airport(airport_code)
);

CREATE TABLE flight_instance(
flight_num flightnum,
date date,
airplane_id char(5) not null,
num_seats smallint not null,
dept_time time,
arr_time time,
PRIMARY KEY(flight_num,date),
FOREIGN KEY (flight_num) REFERENCES flight(flight_num) on DELETE
CASCADE on UPDATE CASCADE 
);

CREATE TABLE fare (
flight_num flightnum,
fare_code char(1),
amount integer not null,
PRIMARY KEY(flight_num,fare_code),
FOREIGN KEY (flight_num) REFERENCES flight(flight_num) on DELETE
CASCADE on UPDATE CASCADE
);

CREATE TABLE seat_reservation(
flight_num flightnum not null,
date date not null,
seat_num aircode not null,
fare_code char(1) not null,
cus_name varchar(30) not null,
cus_phone char(12) not null,
PRIMARY KEY(flight_num,date,seat_num),
FOREIGN KEY (flight_num,date) REFERENCES flight_instance(flight_num,date),
FOREIGN KEY (flight_num,fare_code) REFERENCES fare(flight_num,fare_code)
);

INSERT INTO airplane_type values ('Boeing 737-900','215','Boeing'),('Boeing 737-800','220','Boeing'),('Airbus A330-300','225','Airbus'),
('Airbus A340-500','225','Airbus');

INSERT INTO airplane values ('PKAAA','Airbus A330-300'),('PKAAZ','Airbus A340-500'),('PKBAC','Boeing 737-800'),('PKBBC','Boeing 737-900'); 

INSERT INTO airport values ('CGK','Sukarno‐Hatta International Airport 
','Jakarta','DKI Jakarta','Indonesia'),('DPS','Ngurah Rai International Airport','Denpasar','bali','Indonesia'),('HLP','Halim Perdanakusumah 
','Jakarta','DKI Jakarta','Indonesia'),('PDG','Minangkabau International Airport ','Padang','Sumatera Barat','Indonesia'),('SUB','Juanda','Surabaya','Jawa Timur','Indonesia'),('UPG','Hasanuddin','Makassar','Sulawesi Selatan','Indonesia'),('DIJ','Sentani','Jayapura','Papua','Indonesia'),('SIN','Changi International Airport','Singapore','Singapore','Singapore');  

INSERT INTO flight values ('GA828','Garuda Indonesia','Sabtu','CGK','11:30','SIN','14:20'),('GA654','Garuda Indonesia','Selasa','CGK','5:00','DIJ','13:00'),('JT350','Lion Air 
','Senin','PDG','8:10','CGK','9:55'),('JT561','Lion Air','Senin','DPS','6:00','CGK','8:05'),('JT21','Lion Air','Senin','DPS','13:00','CGK','13:55'),('JT1000','Lion Air','Kamis','SUB','10:00','CGK','12:00'),('GA888','Garuda Indonesia','Jumat','CGK','6:00','UPG','9:00'),('GA889','Garuda Indonesia','Jumat','UPG','10:00','DIJ','14:00');

INSERT INTO flight_instance values ('GA654','2012-04-3','PKAAA','200','5:00','13:00'),('GA654','2012-04-10','PKAAZ','200','5:10','13:15'),('GA828','2012-04-07','PKAAA','210','11:30','14:30'),('GA828','2012-04-14','PKAAA','210','11:40','14:45'),('GA828','2012-05-5','PKAAZ','210',NULL,NULL),('JT350','2012-04-23','PKBAC','200','8:10','10:00'),('JT350','2012-05-8','PKBAC','200',NULL,NULL),('JT561','2012-05-7','PKBAC','210',NULL,NULL),('JT21','2012-05-8','PKBBC','205',NULL,NULL),('JT1000','2012-05-9','PKBBC','205',NULL,NULL);  

INSERT INTO fare values ('GA654','A','4000000'),('GA654','B','4500000'),('GA828','A','700000'),('GA828','B','850000'),('GA888','A','1000000'),('GA888','B','1200000'),('GA889','A','2500000'),('JT350','A','700000'),('JT561','A','850000'),('JT561','B','900000'),('JT21','A','850000'),('JT1000','A','700000');

INSERT INTO seat_reservation values ('GA654','2012-04-3','02A','A','Puspa','081200001111'),('GA654','2012-04-3','10B','B','Tami','081300002222'),('GA828','2012-04-7','21E','A','Yova','081211110000'),('GA828','2012-04-7','21F','B','Puspa','081200001111'),('GA654','2012-04-10','12B','B','Kohak','081200001122'),('GA654','2012-04-10','22S','A','Jade','081211110099'),('GA828','2012-04-14','21C','B','Judas','081200008899')
,('GA828','2012-04-14','21S','A','Anise','081200001133'),('GA828','2012-05-5','21Z','A','Cless','081200001112'),('GA828','2012-05-5','55S','B','Mint','081300003333'),('JT350','2012-04-23','23N','A','Riou','081200001177')
,('JT350','2012-04-23','32S','A','Lion','081200001121'),('JT350','2012-04-23','13D','A','Zidane','081200001100'),('JT350','2012-04-23','17B','A','Tir','081200001137'),('JT350','2012-05-8','23Z','A','Shinn','081200001124'),
('JT350','2012-05-8','01A','A','Athrun','081200001125'),('JT350','2012-05-8','07S','A','Amuro','081200008820'),('JT350','2012-05-8','03D','A','Presea','081255110000'),('JT561','2012-05-7','33E','A','Caius','081200001129'),
('JT561','2012-05-7','23E','B','Lloyd','081200001128'),('JT21','2012-05-8','34D','A','Colette','081200001130'),('JT21','2012-05-8','39I','A','Shing','081200001131'),('JT1000','2012-05-9','37T','A','Luke','081200001132'),
('JT1000','2012-05-9','37U','A','Asch','081200001133'),('JT1000','2012-05-9','37X','A','Reid','081200001164');  

SELECT DISTINCT airline FROM flight,airport
WHERE dept_airport = airport_code AND city = 'Jayapura'; 

SELECT flight_num,airline,weekdays,sch_dept_time FROM flight
WHERE Dept_airport = 'DPS' AND Arr_airport = 'CGK'
ORDER BY flight_num ASC;  						 

SELECT flight_num,weekdays FROM flight 
WHERE dept_airport ='CGK' OR arr_airport = 'CGK';

SELECT flight_num,date,ap.airplane_id FROM flight_instance as ap,airplane as a,airplane_type
WHERE ap.airplane_id = a.airplane_id AND airplane_type = type_name AND company = 'Airbus';

SELECT flight_num,name,airline From flight,airport
WHERE arr_airport = airport_code AND dept_airport in ( SELECT airport_code FROM airport
WHERE name LIKE '%International%');

select E.airline, avg(sum_seats)
from
flight as E left outer join
(select * from (select flight_num, date, count (*) as sum_seats
from flight_instance natural join seat_reservation
group by flight_num, date) as A, flight as B, airport C
where (A.flight_num = B.flight_num and C.name = 'Sukarno-Hatta
International Airport' and A.date = '2012-4-3' and B.dept_airport =
C.airport_code)) as D on E.airline = D.airline
group by E.airline;

SELECT flight_num,date,COUNT(*) as sum_seats
FROM flight_instance NATURAL JOIN seat_reservation 
GROUP BY flight_num,date
HAVING COUNT(*) < 5 ;

SELECT DISTINCT airline
FROM flight_instance as X,flight as Y
WHERE X.flight_num = Y.flight_num
AND dept_time <= sch_dept_time;       
					   
SELECT DISTINCT airline 
FROM flight AS X, airplane AS Y ,flight_instance AS Z
WHERE X.flight_num = Z.flight_num AND Z.airplane_id = Y.airplane_id
AND Y.airplane_type LIKE '%Airbus%' 
EXCEPT (
SELECT DISTINCT airline 
FROM flight AS X, airplane AS Y ,flight_instance AS Z
WHERE Y.airplane_type = '%Boeing%'
);
 
SELECT DISTINCT city
FROM airport
WHERE city NOT IN (
SELECT dept_airport FROM flight
WHERE dept_airport = airport_code
AND name = 'Minangkabau International Airport'
)
AND city NOT IN (
SELECT arr_airport FROM flight
WHERE arr_airport = airport_code
AND name = 'Minangkabau International Airport' 
)
ORDER BY city ASC;

SELECT flight_num,airline,COUNT(seat_num),SUM(amount)
FROM (flight NATURAL JOIN seat_reservation) AS A
NATURAL JOIN  fare
WHERE EXTRACT (MONTH FROM date) = 4
GROUP BY flight_num,airline;
 
SELECT DISTINCT cus_name,cus_phone 
FROM (flight NATURAL JOIN seat_reservation) AS temp
WHERE NOT EXISTS ( SELECT * FROM flight 
               WHERE airline = temp.airline);
			   
UPDATE airplane_type
SET type_name = 'Boeing 737-900ER'
WHERE type_name = 'Boeing 737-900';

DELETE FROM airplane_type
WHERE type_name = 'Airbus A330-300';

DELETE FROM flight
WHERE flight_num = 'GA654';

ALTER TABLE fare
ALTER COLUMN fare_code TYPE char(2);

DROP DOMAIN flightnum CASCADE;