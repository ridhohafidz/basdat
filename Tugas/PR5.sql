CREATE DOMAIN flightnum varchar(6);

ALTER TABLE flight ADD COLUMN flight_num flightnum;
ALTER TABLE flight_instance ADD COLUMN flight_num flightnum;
ALTER TABLE fare ADD COLUMN flight_num flightnum;
ALTER TABLE seat_reservation ADD COLUMN flight_num flightnum;

UPDATE flight set flight_num = 'GA828' where airline ='Garuda Indonesia'
AND sch_dept_time = '11:30';
UPDATE flight set flight_num = 'GA654' where airline ='Garuda Indonesia'
AND sch_dept_time = '5:00';
UPDATE flight set flight_num = 'JT350' where airline ='Lion Air'
AND sch_dept_time = '8:10';
UPDATE flight set flight_num = 'JT561' where airline ='Lion Air'
AND sch_dept_time = '6:00';
UPDATE flight set flight_num = 'JT21' where airline ='Lion Air'
AND sch_dept_time = '13:00';
UPDATE flight set flight_num = 'JT1000' where airline ='Lion Air'
AND sch_dept_time = '10:00';
UPDATE flight set flight_num = 'GA888' where airline ='Garuda Indonesia'
AND sch_dept_time = '6:00';
UPDATE flight set flight_num = 'GA889' where airline ='Garuda Indonesia'
AND sch_dept_time = '10:00';
ALTER TABLE flight ADD PRIMARY KEY (flight_num);

UPDATE flight_instance set flight_num = 'GA654' where date ='2012-04-3';
UPDATE flight_instance set flight_num = 'GA654' where date ='2012-04-10';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-04-07';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-04-14';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-05-5';
UPDATE flight_instance set flight_num = 'JT350' where date ='2012-04-23';
UPDATE flight_instance set flight_num = 'JT350' where date ='2012-05-8'
AND airplane_id = 'PKBAC';
UPDATE flight_instance set flight_num = 'JT561' where date ='2012-05-7';
UPDATE flight_instance set flight_num = 'JT21' where date ='2012-05-8'
AND airplane_id ='PKBBC';
UPDATE flight_instance set flight_num = 'JT1000' where date ='2012-05-9';
ALTER TABLE flight_instance ADD PRIMARY KEY (flight_num,date);
ALTER TABLE flight_instance ADD FOREIGN KEY (flight_num) REFERENCES flight(flight_num)
on UPDATE CASCADE on DELETE CASCADE;

DELETE FROM fare where fare_code = 'A' AND Amount = '700000';
DELETE FROM fare where fare_code = 'A' AND Amount = '850000';
Insert Into fare values('A','700000','JT350'),('A','850000','JT561'),('A','850000','JT21'),('A','700000','JT1000');
UPDATE fare set flight_num = 'GA654' where fare_code = 'A' AND Amount = '4000000';
UPDATE fare set flight_num = 'GA654' where fare_code = 'B' AND Amount = '4500000';
UPDATE fare set flight_num = 'GA828' where fare_code = 'A' AND Amount = '700000';
UPDATE fare set flight_num = 'GA828' where fare_code = 'B' AND Amount = '850000';
UPDATE fare set flight_num = 'GA888' where fare_code = 'A' AND Amount = '1000000';
UPDATE fare set flight_num = 'GA888' where fare_code = 'B' AND Amount = '1200000';
UPDATE fare set flight_num = 'GA889' where fare_code = 'A' AND Amount = '2500000';
UPDATE fare set flight_num = 'JT561' where fare_code = 'B' AND Amount = '900000';
ALTER TABLE fare ADD PRIMARY KEY (flight_num,fare_code);
ALTER TABLE fare ADD FOREIGN KEY (flight_num) REFERENCES flight(flight_num)
on UPDATE CASCADE on DELETE CASCADE;

UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'A'
AND Cus_name = 'Puspa';
UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'A'
AND Cus_name = 'Jade';
UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'B'
AND Cus_name = 'Tami';
UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'B'
AND Cus_name = 'Kohak';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'A'
AND Cus_name = 'Yova';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'A'
AND Cus_name = 'Anise'; 
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'A'
AND Cus_name = 'Cless';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'B'
AND Cus_name = 'Puspa';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'B'
And Cus_name = 'Judas';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'B'
And Cus_name = 'Mint';
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
AND Cus_name = 'Riou';
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Lion';
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Tir'; 
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Shinn'; 
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Zidane'; 
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Athrun';
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Amuro'; 
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
And Cus_name = 'Presea';
UPDATE seat_reservation set flight_num = 'JT561' where fare_code = 'A'
AND Cus_name = 'Caius';
UPDATE seat_reservation set flight_num = 'JT561' where fare_code = 'B'
AND Cus_name = 'Lloyd';
UPDATE seat_reservation set flight_num = 'JT21' where fare_code = 'A'
AND Cus_name = 'Colette';
UPDATE seat_reservation set flight_num = 'JT21' where fare_code = 'A'
And Cus_name = 'Shing';
UPDATE seat_reservation set flight_num = 'JT1000' where fare_code = 'A'
AND Cus_name = 'Luke';
UPDATE seat_reservation set flight_num = 'JT1000' where fare_code = 'A'
And Cus_name = 'Asch';
UPDATE seat_reservation set flight_num = 'JT1000' where fare_code = 'A' 
And Cus_name = 'Reid';
ALTER TABLE seat_reservation ADD PRIMARY KEY (flight_num,date,seat_num);
ALTER TABLE seat_reservation ADD FOREIGN KEY (flight_num,date) REFERENCES flight_instance(flight_num,date)
on UPDATE CASCADE on DELETE CASCADE;
ALTER TABLE seat_reservation ADD FOREIGN KEY (flight_num,fare_code) REFERENCES fare(flight_num,fare_code)
on UPDATE CASCADE on DELETE CASCADE;

CREATE VIEW statistik_waktu AS
SELECT flight_num,AVG(Arr_time-dept_time)as lama_terbang,AVG(Dept_time-sch_dept_time)as delay
FROM (flight NATURAL JOIN flight_instance)as AA 
GROUP BY flight_num;

(3a)
ALTER TABLE flight_instance ADD jumlah_tiket_terjual smallint;
ALTER TABLE flight_instance ADD total_penjualan int ;

(3b)
CREATE OR REPLACE FUNCTION get_jml_tiket_terjual()
RETURNS void AS 
$$
	BEGIN 
		UPDATE flight_instance as fins 
		set jumlah_tiket_terjual = 0;
		
		UPDATE flight_instance as fins 
		set jumlah_tiket_terjual = total.count 
		FROM ( 
			SELECT flight_num,date,count(*) 
			FROM seat_reservation 
			GROUP BY flight_num, date
			) as total 
		WHERE fins.flight_num = total.flight_num 
		AND fins.date = total.date;
	end;
$$
LANGUAGE plpgsql;
SELECT get_jml_tiket_terjual();

(3c)
CREATE OR REPLACE FUNCTION get_total_penjualan() Returns void AS 
$$
BEGIN
     UPDATE flight_instance as fins
     set total_penjualan = 0;

     UPDATE flight_instance as fins
     set total_penjualan = total.sum
     FROM (
          SELECT flight_num,date,sum(Amount)
          FROM seat_reservation
          GROUP BY flight_num,date
         ) as total
	 WHERE fins.flight_num = total.flight_num
     AND fins.date = total.date;
	 end;
$$
LANGUAGE plpgsql;
SELECT get_total_penjualan();

(3d)
CREATE OR REPLACE FUNCTION upd_jml_tiket_terjual() RETURNS TRIGGER AS 
$$
	BEGIN
		If(TG_OP = 'INSERT') THEN
			UPDATE flight_instance as fins 
			SET jumlah_tiket_terjual = jumlah_tiket_terjual + 1
			WHERE fins.flight_num = NEW.flight_num
				AND fins.date = NEW.date;
			RETURN NEW;
			
		elsif(TG_OP = 'UPDATE') THEN 
			UPDATE flight_instance as fins 
			SET jumlah_tiket_terjual = jumlah_tiket_terjual - 1
			WHERE fins.flight_num = OLD.flight_num
				AND fins.date = OLD.date;
			   
			UPDATE flight_instance as fins
			SET jumlah_tiket_terjual = jumlah_tiket_terjual + 1
			WHERE fins.flight_num = NEW.flight_num
				AND fins.date = NEW.date;
			RETURN NEW;
			
		elsif (TG_OP = 'DELETE') THEN
			UPDATE flight_instance as fins 
			SET jumlah_tiket_terjual = jumlah_tiket_terjual - 1
			WHERE fins.flight_num = OLD.flight_num
				AND fins.date = OLD.date;
			RETURN OLD;
		end if;
	end;
$$
LANGUAGE plpgsql;
	
(3e)
CREATE TRIGGER trg_SR_upd_jml_tiket_terjual 
AFTER INSERT OR UPDATE OR DELETE 
ON seat_reservation for each row
EXECUTE PROCEDURE upd_jml_tiket_terjual();

(3f)
CREATE OR REPLACE FUNCTION upd_total_penjualan() returns Trigger as
$$
BEGIN
     If(TG_OP = 'INSERT') THEN
	    UPDATE flight_instance as fins
	    SET jumlah_tiket_terjual = jumlah_tiket_terjual +
		                        fare.amount FROM fare
		WHERE fins.flight_num = NEW.flight_num
	          AND fins.date = NEW.date;
	    RETURN NEW;
	elsif(TG_OP = 'UPDATE') THEN 
	     UPDATE flight_instance as fins 
		 SET jumlah_tiket_terjual = jumlah_tiket_terjual - 
		                         fare.amount FROM fare
         WHERE fins.flight_num = OLD.flight_num
		       AND fins.date = OLD.date;
			   
		 UPDATE flight_instance as fins
	     SET jumlah_tiket_terjual = jumlah_tiket_terjual + 
		                         fare.amount FROM fare
	     WHERE fins.flight_num = NEW.flight_num
	          AND fins.date = NEW.date;
		 RETURN NEW;
     elsif (TG_OP = 'DELETE') THEN
           UPDATE flight_instance as fins 
		   SET jumlah_tiket_terjual = jumlah_tiket_terjual - 
		                           fare.amount FROM fare
           WHERE fins.flight_num = OLD.flight_num
		       AND fins.date = OLD.date;
           RETURN OLD;
    end if;
	end;
$$
LANGUAGE plpgsql;

(3g)
CREATE TRIGGER trg_SR_upd_total_penjualan
AFTER INSERT OR UPDATE OR DELETE 
ON seat_reservation for each row
EXECUTE PROCEDURE upd_total_penjualan();

(3H)
INSERT INTO seat_reservation values ('2012-05-08','09S','A','Kira','081200001222','JT350'),
('2012-05-08','10S','A','Lacus','081200001292','JT350');
(3i)
UPDATE seat_reservation set date = '2012-04-10' Where 
Cus_name = 'Puspa' AND fare_code = 'A';	
(3j)
UPDATE seat_reservation set fare_code = 'A' Where 
Cus_name = 'Tami' AND flight_num = 'GA654';		
(3k)
DELETE FROM seat_reservation where fare_code = 'A' AND Cus_name = 'Yova'; 
