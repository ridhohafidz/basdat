CREATE SCHEMA sirensi;
SET SEARCH_PATH to sirensi;

CREATE TABLE person (
id char(10) not null,
nama varchar(25) not null,
alamat varchar(30) not null,
password varchar (15) not null,
user_name varchar(20) not null,
PRIMARY KEY (id)
);

CREATE TABLE konferensi (
id char(5) not null,
nama varchar(30) not null,
kapasitas integer not null,
cp varchar(30) not null,
tempat varchar(25) not null,
bidang_ilmu varchar(30) not null,
id_penyelenggara char(10),
PRIMARY KEY (id),
FOREIGN KEY (id_penyelenggara) REFERENCES penyelenggara(id) on DELETE
RESTRICT on UPDATE RESTRICT
);

CREATE TABLE penyelenggara (
id char(10) not null,
nama_lembaga varchar(20) not null,
PRIMARY KEY (id)
);

CREATE TABLE makalah (
judul text not null,
status varchar(20) not null,
id_presenter char(10),
PRIMARY KEY (judul),
FOREIGN KEY (id_presenter) REFERENCES peserta(id) on DELETE
RESTRICT on UPDATE RESTRICT
 );
 
CREATE TABLE peserta (
id char(10) not null,
nama_lembaga varchar(20) not null,
pekerjaan varchar(20) not null,
jenis_peserta varchar(10) not null,
is_presenter boolean not null,
nama_hotel varchar(20),
no_kamar char(4),
jmlh_malam smallint,
id_konferensi char(5) not null,
tipe_biaya varchar(15) not null,
file_bukti varchar(20),
tanggal_bayar date,
PRIMARY KEY(id),
FOREIGN KEY (id) REFERENCES person(id) on DELETE
RESTRICT on UPDATE RESTRICT,
FOREIGN KEY (nama_hotel) REFERENCES akomodasi(nama_hotel) on DELETE
RESTRICT on UPDATE RESTRICT,
FOREIGN KEY (id_konferensi) REFERENCES biaya_pendaftaran(id_konferensi) on DELETE
RESTRICT on UPDATE RESTRICT,
FOREIGN KEY (tipe_biaya) REFERENCES biaya_pendaftaran(tipe_biaya) on DELETE
RESTRICT on UPDATE RESTRICT
);

CREATE TABLE biaya_pendaftaran (
id_konferensi char(5) not null,
tipe_biaya varchar(15) not null,
tarif money not null,
PRIMARY KEY (id_konferensi,tipe_biaya)
);

CREATE TABLE akomodasi (
nama_hotel varchar(25) not null,
alamat varchar(30) not null,
biaya_per_malam money not null,
PRIMARY KEY(nama_hotel)
);

CREATE TABLE panitia (
id char(10) not null,
bidang_ilmu varchar(30) not null,
jabatan varchar(20) not null,
is_reviewer boolean not null,
PRIMARY KEY(id),
FOREIGN KEY (id) REFERENCES person(id) on DELETE
RESTRICT on UPDATE RESTRICT
);

CREATE TABLE reviewer_memeriksa_versimakalah(
judul text not null,
nama_versi varchar(10) not null,
id_reviewer char(10) not null,
tgl date not null,
komentar text not null,
PRIMARY KEY(judul,nama_versi,id_reviewer),
FOREIGN KEY (judul,nama_versi) REFERENCES versi(judul,nama) on DELETE
RESTRICT on UPDATE RESTRICT,
FOREIGN KEY (id_reviewer) REFERENCES panitia(id) on DELETE
RESTRICT on UPDATE RESTRICT
);

CREATE TABLE versi (
judul text not null,
nama varchar(10) not null,
tgl_submit date not null,
PRIMARY KEY (judul,nama),
FOREIGN KEY (judul) REFERENCES makalah(judul) on DELETE
RESTRICT on UPDATE RESTRICT
);

 
 
 