CREATE DOMAIN flightnum varchar(6);

ALTER TABLE flight ADD COLUMN flight_num flightnum;
ALTER TABLE flight_instance ADD COLUMN flight_num flightnum;
ALTER TABLE fare ADD COLUMN flight_num flightnum;
ALTER TABLE seat_reservation ADD COLUMN flight_num flightnum;

UPDATE flight set flight_num = 'GA828' where airline ='Garuda Indonesia'
AND sch_arr_time = '14:20';
UPDATE flight set flight_num = 'GA654' where airline ='Garuda Indonesia'
AND sch_arr_time = '13:00';
UPDATE flight set flight_num = 'JT350' where airline ='Lion Air'
AND sch_arr_time = '9:55';
UPDATE flight set flight_num = 'JT561' where airline ='Lion Air'
AND sch_arr_time = '8:05';
UPDATE flight set flight_num = 'JT21' where airline ='Lion Air'
AND sch_arr_time = '13:55';
UPDATE flight set flight_num = 'JT1000' where airline ='Lion Air'
AND sch_arr_time = '12:00';
UPDATE flight set flight_num = 'GA888' where airline ='Garuda Indonesia'
AND sch_arr_time = '9:00';
UPDATE flight set flight_num = 'GA889' where airline ='Garuda Indonesia'
AND sch_arr_time = '14:00';

UPDATE flight_instance set flight_num = 'GA564' where date ='2012-04-3'
AND arr_time = '13:00';
UPDATE flight_instance set flight_num = 'GA654' where date ='2012-04-10'
AND arr_time = '13:15';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-04-07'
AND arr_time = '14:30';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-04-14'
AND arr_time = '14:45';
UPDATE flight_instance set flight_num = 'GA828' where date ='2012-05-5'
AND arr_time = null;
UPDATE flight_instance set flight_num = 'JT350' where date ='2012-04-23'
AND arr_time = '10:00';
UPDATE flight_instance set flight_num = 'JT350' where date ='2012-05-8'
AND arr_time = null;
UPDATE flight_instance set flight_num = 'JT561' where date ='2012-05-7'
AND arr_time = null;
UPDATE flight_instance set flight_num = 'JT21' where date ='2012-05-8'
AND arr_time = null;
UPDATE flight_instance set flight_num = 'JT1000' where date ='2012-04-9'
AND arr_time = null;

UPDATE fare set flght_num = 'GA654' where fare_code = 'A' AND Amount = '4000000';
UPDATE fare set flght_num = 'GA654' where fare_code = 'B' AND Amount = '4500000';
UPDATE fare set flght_num = 'GA828' where fare_code = 'A' AND Amount = '700000';
UPDATE fare set flght_num = 'GA828' where fare_code = 'B' AND Amount = '850000';
UPDATE fare set flght_num = 'GA888' where fare_code = 'A' AND Amount = '1000000';
UPDATE fare set flght_num = 'GA888' where fare_code = 'B' AND Amount = '1200000';
UPDATE fare set flght_num = 'GA889' where fare_code = 'A' AND Amount = '2500000';
UPDATE fare set flght_num = 'JT350' where fare_code = 'A' AND Amount = '700000';
UPDATE fare set flght_num = 'JT561' where fare_code = 'A' AND Amount = '850000';
UPDATE fare set flght_num = 'JT561' where fare_code = 'B' AND Amount = '900000';
UPDATE fare set flght_num = 'JT21' where fare_code = 'A' AND Amount = '850000';
UPDATE fare set flght_num = 'JT1000' where fare_code = 'A' AND Amount = '700000';

UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'A'
AND name = 'Puspa' OR name = 'Jade';
UPDATE seat_reservation set flight_num = 'GA654' where fare_code = 'B'
AND name = 'Tami' OR name = 'Kohak';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'A'
AND name = 'Yova' OR name = 'Anise'OR name = 'Cless';
UPDATE seat_reservation set flight_num = 'GA828' where fare_code = 'B'
AND name = 'Puspa' OR name = 'Judas' OR name = 'Mint';
UPDATE seat_reservation set flight_num = 'JT350' where fare_code = 'A'
AND name = 'Tami' OR name = 'Kohak';





