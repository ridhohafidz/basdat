CREATE SCHEMA PTKAI;
SET SEARCH_PATH to PTKAI;

CREATE DOMAIN idkereta char(5);

CREATE DOMAIN idstasiun char(5);

CREATE TABLE KERETA(
id_kereta idkereta,
nama varchar(15) not null,
max_seats Int not null,
kelas varchar(10) not null,
PRIMARY KEY(id_kereta)
);

CREATE TABLE STASIUN(
id_stasiun idstasiun not null,
nama varchar(15) not null,
kota char(10) not null,
provinsi varchar(15) not null,
PRIMARY KEY (id_stasiun)
);

CREATE TABLE JADWAL(
id_jadwal char(8) not null,
tgl date not null,
id_kereta idkereta not null,
stasiun_berangkat idkereta not null,
stasiun_tiba idkereta not null,
jam_berangkat time,
jam_tiba time,
PRIMARY KEY (id_jadwal),
FOREIGN KEY (id_kereta) REFERENCES KERETA(id_kereta)on DELETE
CASCADE on UPDATE CASCADE,
FOREIGN KEY (stasiun_berangkat) REFERENCES STASIUN(id_stasiun)on DELETE
RESTRICT on UPDATE CASCADE,
FOREIGN KEY (stasiun_tiba) REFERENCES STASIUN(id_stasiun)on DELETE
RESTRICT on UPDATE CASCADE 
);

CREATE TABLE HARGA_TIKET(
id_jadwal char(8) not null,
jenis_penumpang char(1) not null,
harga int not null,
PRIMARY KEY (id_jadwal,jenis_penumpang),
FOREIGN KEY (id_jadwal) REFERENCES JADWAL(id_jadwal)
);

CREATE TABLE PEMESANAN(
id_jadwal char(8) not null,
no_bangku char(3) not null,
gerbong char(1) not null,
jenis_penumpang char(1) not null,
nama_penumpang char(10) not null,
tlp_penumpang int not null,
PRIMARY KEY(id_jadwal,no_bangku,gerbong,jenis_penumpang),
FOREIGN KEY (id_jadwal,jenis_penumpang) REFERENCES HARGA_TIKET(id_jadwal,jenis_penumpang)
);

INSERT INTO KERETA values ('KT123','Bima','200','Executive'),('KT456','Mutiara','300','AC-Economy'),('KT789','Senja','400','Economy'),
('KT111','Rajawali','400','AC-Economy');

INSERT INTO STASIUN values ('STS01','Jakarta Kota','Jakarta','DKI Jakarta'),('STS02','Stasiun Gambir','Gambir','Jawa Barat'),('STS03','Balapan','Solo','Jawa Tengah'),('STS04','Stasiun Semut','Surabaya','Jawa Timur')
,('STS05','Stasiun Tugu','Yogyakarta','DIY'); 

INSERT INTO JADWAL values ('0001','2012-11-11','KT123','STS01','STS04','10:00','18:00'),('0002','2012-11-12','KT789','STS05','STS02','11:00','22:00'),
('0003','2012-11-12','KT456','STS04','STS05','19:00','08:00'),('0004','2012-11-14','KT111','STS02','STS03','16:00','06:00'),('0005','2012-11-15','KT789','STS03','STS02','13:00',NULL),('0006','2012-11-18','KT111','STS03','STS05','09:00',NULL),('0007','2012-11-20','KT123','STS04','STS01','17:00',NULL)
,('0008','2012-11-20','KT789','STS01','STS04',NULL,NULL),('0009','2012-11-20','KT111','STS03','STS02',NULL,NULL),('0010','2012-11-22','KT456','STS05','STS03',NULL,NULL);

INSERT INTO HARGA_TIKET values ('0001','D','250000'),('0001','A','230000'),('0002','D','240000'),('0002','A','140000'),('0003','D','300000'),('0003','A','200000')
,('0006','D','200000'),('0006','A','150000'),('0007','D','300000'),('0008','D','150000'),('0009','A','130000'),('0010','D','200000'),('0010','A','180000');  

INSERT INTO PEMESANAN values ('0001','12A','3','D','Ani','021234123'),('0002','21D','2','D','Ana','012314712'),('0003','18F','3','A','Budi','081823728')
,('0003','23C','6','D','Toni','018232323'),('0010','9E','4','A','Tono','088234913'),('0009','10B','5','A','Mila','012121212');  

a) SELECT DISTINCT k.nama
FROM KERETA k,STASIUN s,JADWAL j
WHERE k.id_kereta=j.id_kereta
AND s.id_stasiun=j.stasiun_tiba
AND s.kota='Surabaya';

b) SELECT k.nama,k.kelas
FROM KERETA k,STASIUN s,JADWAL j
WHERE k.id_kereta=j.id_kereta
AND s.id_stasiun=j.stasiun_berangkat
AND tgl='2012-11-12';

c)SELECT k.nama,j.tgl,j.jam_berangkat
FROM KERETA k,STASIUN s,JADWAL j
WHERE k.id_kereta=j.id_kereta
AND s.id_stasiun=j.stasiun_berangkat
AND s.id_stasiun=j.stasiun_tiba
AND j.stasiun_berangkat='STS02'
AND j.stasiun_tiba='STS04';

d)SELECT h.harga
FROM KERETA k,JADWAL j,HARGA_TIKET h
WHERE k.id_kereta=j.id_kereta
AND j.id_jadwal=h.id_jadwal
AND j.tgl='2012-11-20'
AND h.jenis_penumpang='D';

e)SELECT k.nama,j.stasiun_tiba
FROM KERETA k,JADWAL j,STASIUN s
WHERE k.id_kereta=j.id_kereta
AND s.id_stasiun=j.stasiun_berangkat
AND s.nama='Stasiun Gambir';

f)SELECT*
FROM JADWAL j
WHERE j.id_kereta In(SELECT k.id_kereta
                     FROM KERETA k,(PEMESANAN NATURAL JOIN HARGA_TIKET)h
                     WHERE j.id_jadwal=h.id_jadwal
					 GROUP BY k.id_kereta,k.max_seats
					 HAVING SUM(h.harga)<0.5*max_seats 
					 );
					 
g)SELECT AVG(h.harga)AS Average
FROM KERETA k,JADWAL j,HARGA_TIKET h
WHERE k.id_kereta=j.id_kereta
AND j.id_jadwal=h.id_jadwal
AND j.stasiun_berangkat='STS02'
AND j.stasiun_tiba='STS05'
AND h.jenis_penumpang='D';

h)SELECT k.nama,COUNT(h.id_jadwal),SUM(h.harga)
  FROM KERETA k,JADWAL j,HARGA_TIKET h,PEMESANAN p
  WHERE k.id_kereta=j.id_kereta
  AND j.id_jadwal=h.id_jadwal
  AND h.id_jadwal=p.id_jadwal
  GROUP BY k.nama;
  
i)SELECT p.nama_penumpang,p.tlp_penumpang
FROM JADWAL j,HARGA_TIKET h,PEMESANAN p
WHERE j.id_jadwal=h.id_jadwal
AND h.id_jadwal=p.id_jadwal
GROUP BY p.nama_penumpang,p.tlp_penumpang
HAVING COUNT(j.id_jadwal)>10;

j)SELECT SUM(max_seats)
FROM KERETA k,STASIUN s,JADWAL j
WHERE k.nama='Bima'
AND j.stasiun_berangkat='STS01'
AND j.stasiun_tiba='STS02'
AND k.id_kereta=j.id_kereta;

k)SELECT k.nama
FROM KERETA k,JADWAL j
WHERE k.id_kereta=j.id_kereta
AND j.jam_berangkat='19:00';

l)SELECT DISTINCT k.nama
FROM KERETA k,JADWAL j,HARGA_TIKET h
WHERE k.id_kereta=j.id_kereta
AND j.id_jadwal=h.id_jadwal
AND h.jenis_penumpang='A';


a)UPDATE KERETA
SET id_kereta = 'KT890'
WHERE id_kereta = 'KT789';

b)DELETE FROM KERETA
WHERE id_kereta = 'KT111';

c)DELETE FROM STASIUN
WHERE nama = 'Stasiun Tugu';

ALTER TABLE PEMESANAN
ALTER COLUMN gerbong TYPE char(2);

d)DROP DOMAIN idkereta CASCADE;